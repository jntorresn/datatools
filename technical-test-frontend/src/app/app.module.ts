import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Routes, RouterModule } from '@angular/router';
import { MainViewComponent } from './main-view/main-view.component';
import { DesignComponent } from './design/design.component';
import { Query2Component } from './query2/query2.component';
import { AdministratorViewComponent } from './administrator-view/administrator-view.component';


const router: Routes = [
  {path: "", component: MainViewComponent},
  {path: "design", component: DesignComponent},
  {path: "point2", component: Query2Component},
  {path: "admin", component: AdministratorViewComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    AdministratorViewComponent,
    MainViewComponent,
    DesignComponent,
    Query2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    RouterModule.forRoot(router),   
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
