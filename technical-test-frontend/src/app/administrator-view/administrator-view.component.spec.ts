import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministratorViewComponent } from './administrator-view.component';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { RouterTestingModule } from '@angular/router/testing';

describe('AdministratorViewComponent', () => {
  let component: AdministratorViewComponent;
  let fixture: ComponentFixture<AdministratorViewComponent>;

  let httpClient : HttpClient;
  let httpTestingController : HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministratorViewComponent ],
      imports: [ 
        HttpClientTestingModule,
        RouterTestingModule ]
    })
    .compileComponents();
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministratorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
