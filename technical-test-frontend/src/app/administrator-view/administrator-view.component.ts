import { Component, OnInit } from '@angular/core';
import { Company } from "../company"
import { Form } from "../form"
import { CompanyService } from '../company.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-administrator-view',
  templateUrl: './administrator-view.component.html',
  styleUrls: ['./administrator-view.component.css'],
  providers: [CompanyService]
})
export class AdministratorViewComponent implements OnInit {

  companies: Company[];

  form: Form = new Form();

  constructor(private companyService: CompanyService, private router : Router) { }

  ngOnInit(): void {

    this.getCompanies();
  }

  private getCompanies() {

    this.companyService.getCompanies().subscribe(data => {

      console.log(data);
      this.companies = data;
    });
  }

  public onSubmit() {
    
    this.saveCompany();
  }

  private saveCompany() {
    
    this.companyService.saveCompany(this.form).subscribe(data => {

      console.log(data);
      this.refresh();
    }, error => {
      console.log(error);
    });    
  }

  private refresh() {

    console.log("refreshed"),    
    this.getCompanies();
  }

}
