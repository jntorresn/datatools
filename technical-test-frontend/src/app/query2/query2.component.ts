import { Component, OnInit } from '@angular/core';
import { Vehicle } from "../vehicle"
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-query2',
  templateUrl: './query2.component.html',
  styleUrls: ['./query2.component.css'],
  providers: [CompanyService]
})
export class Query2Component implements OnInit {

  vehicles : Vehicle[];

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {

    this.getVehicles();
  }

  private getVehicles() {

    this.companyService.getVehicle().subscribe(data => {

      console.log(data);
      this.vehicles = data;
    });
  }
}