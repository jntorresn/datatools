import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.css']
})
export class DesignComponent implements OnInit {

  diagram = "assets/img/diagram.png";
  db = "assets/img/database.png";

  constructor() { }

  ngOnInit(): void {
  }

}
