export class Vehicle {

    plate: string;
    companyIdType: string;
    companyIdNumber: number;
    companyName: string;
    drivers: number;
}
