import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Company } from './company';
import { Vehicle } from './vehicle';
import { Form } from './form';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private companies = "/api/companies";
  private vehicles = "/api/point2";

  constructor(private httpClient: HttpClient) { }

  getCompanies(): Observable<Company[]> {

    return this.httpClient.get<Company[]>(`${this.companies}`);

  }

  getVehicle(): Observable<Vehicle[]> {

    return this.httpClient.get<Vehicle[]>(`${this.vehicles}`);

  }

  saveCompany(form: Form): Observable<Object> {

    return this.httpClient.post(`${this.companies}`, form);
  }

}