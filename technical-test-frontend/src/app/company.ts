export class Company {

    id: string;
    name: string;
    representative_name: string;
    company_type: string;
    id_type: string;
    id_number: string;
    company_name: string;
    address: string;
    city: string;
    department: string;
    country: string;
    phone: string;
}
