export class Form {

    companyType : string;
    companyIdType : string;
    companyID : number;
    companyName : string;
    companyAddress : string;
    companyCity : string;
    companyDepartment : string;
    companyCountry : string;
    companyPhone : number;

    representativeIdType : string;
    representativeID : number;
    representativeName : string;
    representativeAddress : string;
    representativeCity : string;
    representativeDepartment : string;
    representativeCountry : string;
    representativePhone : number;
}
