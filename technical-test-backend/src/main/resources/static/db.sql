DROP DATABASE IF EXISTS datatools;

CREATE DATABASE IF NOT EXISTS datatools;

USE datatools;

CREATE TABLE administrator(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO administrator(name) VALUES ('administrator');

CREATE TABLE legalrepresentative(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  id_type VARCHAR(50) NOT NULL,
  id_number INT UNSIGNED NOT NULL UNIQUE,
  representative_name VARCHAR(50) NOT NULL UNIQUE,
  address VARCHAR(50) NOT NULL,
  city VARCHAR(50) NOT NULL,
  department VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  phone INT UNSIGNED NOT NULL
);

INSERT INTO legalrepresentative(id_type, id_number, representative_name, address, city, department, country, phone) VALUES 
('CC', 79123654, 'Hank Pym', 'Carrera 1 #2-3','San Francisco', 'California', 'USA', 1234567),
('CC', 52678098, 'Tony Stark', 'Calle 3 #2-1','LA', 'California', 'USA', 9876643),
('CC', 52678056, 'Bruce Wayne', 'Calle 3 #2-1','Gotham', 'New York', 'USA', 9876643);

CREATE TABLE company(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  id_admin INT UNSIGNED NOT NULL,
  id_legalrep INT UNSIGNED NOT NULL,
  company_type ENUM('Transporte','Vehículos', 'Conductores') NOT NULL,
  id_type VARCHAR(50) NOT NULL,
  id_number INT UNSIGNED NOT NULL UNIQUE,
  company_name VARCHAR(50) NOT NULL UNIQUE,
  address VARCHAR(50) NOT NULL,
  city VARCHAR(50) NOT NULL,
  department VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  phone INT UNSIGNED NOT NULL,

  FOREIGN KEY (id_admin) REFERENCES administrator(id),
  FOREIGN KEY (id_legalrep) REFERENCES legalrepresentative(id)
);

INSERT INTO company(id_admin, id_legalrep, company_type, id_type, id_number, company_name, address, city, department, country, phone) VALUES 
(1, 1, 'Transporte', 'NIT', 999999, 'Pym Technologies', 'Calle 20 #78-2', 'San Francisco', 'California', 'USA', 2222222),
(1, 2, 'Conductores', 'NIT', 533232, 'Stark Industries', 'Carrera 90 #76-29', 'LA', 'California', 'USA', 9999999),
(1, 3, 'Vehículos', 'NIT', 533266, 'Wayne Enterprises', 'Carrera 90 #76-29', 'Gotham', 'New York', 'USA', 9999999);

CREATE TABLE user(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  id_company INT UNSIGNED NOT NULL,
  user_name VARCHAR(50) NOT NULL,

  FOREIGN KEY (id_company) REFERENCES company(id)
);

INSERT INTO user(id_company, user_name) VALUES (1, 'user1');


CREATE TABLE vehicle(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  id_user INT UNSIGNED NOT NULL,
  id_company INT UNSIGNED NOT NULL,
  affiliated ENUM('SI','NO'),
  plates VARCHAR(50) NOT NULL UNIQUE,
  motor VARCHAR(50) NOT NULL,
  chassis VARCHAR(50) NOT NULL,
  year INT UNSIGNED NOT NULL,
  enrollment_date DATE NOT NULL,
  seated_passangers INT UNSIGNED NOT NULL,
  standing_passangers INT UNSIGNED NOT NULL,
  dry_weight INT UNSIGNED NOT NULL,
  gross_weight INT UNSIGNED NOT NULL,
  doors INT UNSIGNED NOT NULL,
  brand VARCHAR(50) NOT NULL,
  line VARCHAR(50) NOT NULL,

  FOREIGN KEY (id_user) REFERENCES user(id),
  FOREIGN KEY (id_company) REFERENCES company(id)
);

INSERT INTO vehicle(id_user, id_company, affiliated, plates, motor, chassis, year, enrollment_date, seated_passangers, standing_passangers, dry_weight, gross_weight, doors, brand, line) VALUES 
(1, 3, 'SI', 'AJO980', 'TYUI', '23456GHJ', '2019', '2020-7-10', 8, 1, 90, 70, 4, 'TOYOTA', 'Captiva'),
(1, 3, 'SI', 'AJO981', 'TYUI', '23456GHJ', '2019', '2020-7-10', 5, 0, 90, 70, 4, 'FORD', 'Edge'),
(1, 3, 'SI', 'AJO987', 'TYUI', '23456GHJ', '2019', '2020-7-10', 5, 0, 90, 70, 4, 'MAZDA', '10');

CREATE TABLE driver(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  id_user INT UNSIGNED NOT NULL,
  id_company INT UNSIGNED NOT NULL,
  id_type VARCHAR(50) NOT NULL,
  id_number INT UNSIGNED NOT NULL UNIQUE,
  driver_name VARCHAR(50) NOT NULL UNIQUE,
  address VARCHAR(50) NOT NULL,
  city VARCHAR(50) NOT NULL,
  department VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  phone INT UNSIGNED NOT NULL,

  FOREIGN KEY (id_user) REFERENCES user(id),
  FOREIGN KEY (id_company) REFERENCES company(id)
);

INSERT INTO driver(id_user, id_company, id_type, id_number, driver_name, address, city, department, country, phone) VALUES 
(1, 2, 'CC', 98765432,'Steve Rogers', 'Carrera 96 #16-15', 'Bogota', 'Cundinamarca', 'Colombia', 1234567),
(1, 2, 'CC', 98767432, 'Peter Parker', 'Carrera 91 #16-15', 'Bogota', 'Cundinamarca', 'Colombia', 1234567),
(1, 2, 'CC', 98712432, 'Ben Grim', 'Carrera 80 #16-15', 'Bogota', 'Cundinamarca', 'Colombia', 1234567),
(1, 2, 'CC', 98723432, 'Reed Richards', 'Carrera 76 #16-15', 'Bogota', 'Cundinamarca', 'Colombia', 1234567);

CREATE TABLE vehicledriver(

  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  id_driver INT UNSIGNED NOT NULL,
  id_vehicle INT UNSIGNED NOT NULL,

  FOREIGN KEY (id_driver) REFERENCES driver(id),
  FOREIGN KEY (id_vehicle) REFERENCES vehicle(id)
);

INSERT INTO vehicledriver(id_driver, id_vehicle) VALUES
(1,1),
(1,2),
(2,2),
(2,3),
(3,1),
(4,3);