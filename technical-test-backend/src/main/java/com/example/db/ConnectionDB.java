package com.example.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class ConnectionDB {

	private static final String host = "localhost";
	private static final String port = "3306";
	private static final String name = "datatools";
	private static final String user = "root";
	private static final String pass = "";

	private Connection mySQLConnection;

	private void openDB() {

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			this.mySQLConnection = DriverManager.getConnection(
					"jdbc:mysql://" + host + ":" + port + "/" + name + "?useUnicode=true&useserverTimezone=UTC", user,
					pass);

		} catch (Exception error) {

			System.out.println(error);
		}
	}

	private void closeDB() {

        try {

            this.mySQLConnection.close();

        } catch (Exception error) {

            System.out.println(error);
        }
	}

	protected void setQuery(String query) {

        this.openDB();
        
        try {

            Statement myStatement = this.mySQLConnection.createStatement();

            myStatement.executeUpdate(query);
            
        } catch (Exception error) {

            System.out.println(error);
        }

        this.closeDB();

	}

	protected ArrayList<HashMap<String, String>> getQuery(String query) {

        ArrayList<HashMap<String, String>> queryResult = new ArrayList<HashMap<String, String>>();

        this.openDB();

        try {

            Statement myStatement = this.mySQLConnection.createStatement();

            ResultSet resultQuery = myStatement.executeQuery(query);
            ResultSetMetaData resultMetaData = resultQuery.getMetaData();

            while (resultQuery.next()) {

                HashMap<String, String> queryObject = new HashMap<String, String>();

                for (int counter = 0; counter < resultMetaData.getColumnCount(); counter++) {
                    
                    queryObject.put(resultMetaData.getColumnName(counter + 1), resultQuery.getString(resultMetaData.getColumnName(counter + 1)));                    
                }
                
                queryResult.add(queryObject);                
            }

        } catch (Exception error) {

            System.out.println(error);
        }

        this.closeDB();
//        
		return queryResult;
	}

}
