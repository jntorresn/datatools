package com.example.springboot.controller;

import java.util.ArrayList;
import java.util.HashMap;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.model.Company;
import com.example.springboot.model.LegalRepresentative;
import com.example.springboot.model.Vehicle;
import com.example.springboot.model.Vehicle.Response;


@RestController
@RequestMapping("/api")
public class CompanyController {

	// getting companies
	@GetMapping("/companies")
	public ArrayList<HashMap<String, String>> getAdmins() {
		return Company.getCompanies();
	}

	// getting Point 2
	@GetMapping("/point2")
	public ArrayList<Response> getPoint2() {
		Vehicle vehicle = new Vehicle();
		return vehicle.getPoint2();
	}
	
	// Creating Companies
	@PostMapping("/companies")
	public void createCompany(@RequestBody HashMap<String, String> form) {
		
		Company company = new Company(form);
		
	}
}
