package com.example.springboot.model;

import com.example.springboot.crud.RepresentativeCrud;

public class LegalRepresentative {
	
	private int id;
	private String idType;
	private int idNumber;
	private String name;
	private String address;
	private String city;
	private String department;
	private String country;
	private long phone;
	
	public LegalRepresentative (String idType, int idNumber, String address, String city, String department, String country, String name, long phone) {
		
		this.idType = idType;
		this.idNumber = idNumber;
		this.name = name;
		this.address = address;
		this.city = city;
		this.department = department;
		this.country = country;
		this.phone = phone;
		
		this.id = this.saveRepresentative();
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public int getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	
	public int saveRepresentative () {
		
		RepresentativeCrud crud = new RepresentativeCrud();
		
		crud.saveRepresentative(this.idType, this.idNumber, this.name, this.address, this.city, this.department, this.country, this.phone);
		
		return crud.getRepresentativeId(this.idType, this.idNumber);	

	}
	

}
