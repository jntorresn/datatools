package com.example.springboot.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.springboot.crud.VehicleCrud;

public class Vehicle {

	private int id;
	private String user;
	private String affiliated;
	private String plates;
	private String motor;
	private String chassis;
	private int year;
	private String enrollment_date;
	private int seated_passangers;
	private int standing_passangers;
	private int dry_weight;
	private int gross_weight;
	private int doors;
	private String brand;
	private String line;
	private Company company;
	private ArrayList<Driver> drivers;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAffiliated() {
		return affiliated;
	}

	public void setAffiliated(String affiliated) {
		this.affiliated = affiliated;
	}

	public String getPlates() {
		return plates;
	}

	public void setPlates(String plates) {
		this.plates = plates;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getEnrollment_date() {
		return enrollment_date;
	}

	public void setEnrollment_date(String enrollment_date) {
		this.enrollment_date = enrollment_date;
	}

	public int getSeated_passangers() {
		return seated_passangers;
	}

	public void setSeated_passangers(int seated_passangers) {
		this.seated_passangers = seated_passangers;
	}

	public int getStanding_passangers() {
		return standing_passangers;
	}

	public void setStanding_passangers(int standing_passangers) {
		this.standing_passangers = standing_passangers;
	}

	public int getDry_weight() {
		return dry_weight;
	}

	public void setDry_weight(int dry_weight) {
		this.dry_weight = dry_weight;
	}

	public int getGross_weight() {
		return gross_weight;
	}

	public void setGross_weight(int gross_weight) {
		this.gross_weight = gross_weight;
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ArrayList<Driver> getDrivers() {
		return drivers;
	}

	public void setDrivers(ArrayList<Driver> drivers) {
		this.drivers = drivers;
	}

	public class Response {

		public String plate;
		public String companyIdType;
		public int companyIdNumber;
		public String companyName;
		public int drivers;
	}

	public ArrayList<Response> getPoint2() {

		VehicleCrud crud = new VehicleCrud();

		ArrayList<HashMap<String, String>> rows = crud.getPoint2();
		ArrayList<Response> responses = new ArrayList<Response>();

		for (HashMap<String, String> element : rows) {

			Response response = new Response();
			
			response.plate = element.get("plates");
			response.companyIdType = element.get("id_type");
			response.companyIdNumber = Integer.parseInt(element.get("id_number"));
			response.companyName = element.get("company_name");
			response.drivers = Integer.parseInt(element.get("COUNT(vehicledriver.id_vehicle)"));
		
			responses.add(response);
		}	

		return responses;
	}

}
