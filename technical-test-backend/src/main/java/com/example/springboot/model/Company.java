package com.example.springboot.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.springboot.crud.CompanyCrud;

public class Company {

	private int id;
	private Administrator admin;
	private LegalRepresentative legalRep;
	private String type;
	private String idType;
	private int idNumber;
	private String name;
	private String address;
	private String city;
	private String department;
	private String country;
	private long phone;
	
	public Company (HashMap<String,String> form) {
		
		this.admin = new Administrator();
		
		this.legalRep = new LegalRepresentative(
				form.get("representativeIdType"),
				Integer.parseInt(form.get("representativeID")),
				form.get("representativeAddress"),
				form.get("representativeCity"),
				form.get("representativeDepartment"),
				form.get("representativeCountry"),
				form.get("representativeName"),
				Long.parseLong(form.get("representativePhone")));
		
		this.type = form.get("companyType");		
		this.idType = form.get("companyIdType");
		this.idNumber = Integer.parseInt(form.get("companyID"));
		this.name = form.get("companyName");
		this.address = form.get("companyAddress");
		this.city = form.get("companyCity");
		this.department = form.get("companyDepartment");
		this.country = form.get("companyCountry");
		this.phone = Integer.parseInt(form.get("companyPhone"));
		
		this.saveCompany(this.admin.getId(), this.legalRep.getId(), this.type, this.idType, this.idNumber, this.name, this.address, this.city, this.department, this.country, this.phone);
	
	}
			


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Administrator getAdmin() {
		return admin;
	}

	public void setAdmin(Administrator admin) {
		this.admin = admin;
	}

	public LegalRepresentative getLegalRep() {
		return legalRep;
	}

	public void setLegalRep(LegalRepresentative legalRep) {
		this.legalRep = legalRep;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public int getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public static ArrayList<HashMap<String, String>> getCompanies() {

		CompanyCrud crud = new CompanyCrud();

		return crud.getCompanies();
	}
	
	public void saveCompany(int adminID, int legalRepID, String type, String idType, int idNumber, String name, String address, String city, String department, String country, long phone) {
	
		CompanyCrud crud = new CompanyCrud();
		
		crud.saveNewCompany(adminID, legalRepID, type, idType, idNumber, name, address, city, department, country, phone);
	}

}
