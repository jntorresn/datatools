package com.example.springboot.model;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.springboot.crud.AdministratorCrud;

public class Administrator {

	private int id;
	private String name;
	
	public Administrator () {
		
		AdministratorCrud crud = new AdministratorCrud();		
		
		this.id = Integer.parseInt(crud.getAdmins().get(0).get("id"));
		this.name = crud.getAdmins().get(0).get("name");		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
