package com.example.springboot.crud;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.db.ConnectionDB;

public class AdministratorCrud extends ConnectionDB {

	private ArrayList<HashMap<String, String>> rows;

	public ArrayList<HashMap<String, String>> getAdmins() {
		
		String SQLString = "SELECT * FROM administrator";
		
		this.rows = this.getQuery(SQLString); 
		
		return this.rows;
	}

}
