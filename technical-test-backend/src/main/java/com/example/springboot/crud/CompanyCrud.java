package com.example.springboot.crud;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.db.ConnectionDB;

public class CompanyCrud extends ConnectionDB {

	private ArrayList<HashMap<String, String>> rows;

	public ArrayList<HashMap<String, String>> getCompanies() {

		String SQLString = "SELECT company.id, "
				+ "administrator.name, "
				+ "legalrepresentative.representative_name, "
				+ "company.company_type, "
				+ "company.id_type, "
				+ "company.id_number, "
				+ "company.company_name, "
				+ "company.address, company.city, "
				+ "company.department, "
				+ "company.country, "
				+ "company.phone "
				+ "FROM company "
				+ "INNER JOIN administrator ON company.id_admin = administrator.id "
				+ "INNER JOIN legalrepresentative ON company.id_legalrep = legalrepresentative.id";

		this.rows = this.getQuery(SQLString);

		return this.rows;
	}
	
	public void saveNewCompany(int adminID, int legalRepID, String type, String idType, int idNumber, String name, String address, String city, String department, String country, long phone) {
		
		String SQLString = "INSERT INTO company("
				+ "id_admin, "
				+ "id_legalRep, "
				+ "company_type, "
				+ "id_type, "
				+ "id_number, "
				+ "company_name, "
				+ "address, "
				+ "city, "
				+ "department, "
				+ "country, "
				+ "phone) VALUES "
				+ "('" + adminID + "', "
				+ legalRepID + ", "
				+ "'" + type + "', "
				+ "'" + idType + "', "
				+ idNumber + ", "
				+ "'" + name + "', "
				+ "'" + address + "', "
				+ "'" + city + "', "
				+ "'" + department + "', "
				+ "'" + country + "', "
				+ phone + ");";
		
		System.out.println(SQLString);
		
		this.setQuery(SQLString);
		
	}

}
