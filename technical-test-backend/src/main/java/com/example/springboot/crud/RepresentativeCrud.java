package com.example.springboot.crud;

import com.example.db.ConnectionDB;

public class RepresentativeCrud extends ConnectionDB{
	
	public void saveRepresentative (String idType, int idNumber, String name, String address, String city, String department, String country, long phone) {
		
		String SQLString = "INSERT INTO legalrepresentative("
				+ "id_type, "
				+ "id_number, "
				+ "representative_name, "
				+ "address, "
				+ "city, "
				+ "department, "
				+ "country, "
				+ "phone) VALUES "
				+ "('" + idType + "', "
				+ idNumber + ", "
				+ "'" + name + "', "
				+ "'" + address + "', "
				+ "'" + city + "', "
				+ "'" + department + "', "
				+ "'" + country + "', "
				+ phone + ");";
		
		this.setQuery(SQLString);
	}
	
	public int getRepresentativeId (String idType, int idNumber) {
		
		String SQLString = "SELECT legalrepresentative.id " + 
				"FROM legalrepresentative " + 
				"WHERE (legalrepresentative.id_type = '" + idType + "' AND legalrepresentative.id_number = " + idNumber + ")";
		
		return Integer.parseInt(this.getQuery(SQLString).get(0).get("id"));
	}

}
