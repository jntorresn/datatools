package com.example.springboot.crud;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.db.ConnectionDB;

public class VehicleCrud extends ConnectionDB{
	
	private ArrayList<HashMap<String, String>> rows;
	
	public ArrayList<HashMap<String, String>> getPoint2() {		

		String SQLString = "SELECT vehicle.plates, "
				+ "company.id_type, "
				+ "company.id_number, "
				+ "company.company_name, "
				+ "COUNT(vehicledriver.id_vehicle) "
				+ "FROM vehicle "
				+ "INNER JOIN company ON vehicle.id_company = company.id "
				+ "INNER JOIN vehicledriver ON vehicle.id = vehicledriver.id_vehicle "
				+ "INNER JOIN driver ON vehicledriver.id_driver = driver.id "
				+ "GROUP BY vehicledriver.id_vehicle "
				+ "HAVING COUNT(vehicledriver.id_vehicle) > 1 "
				+ "ORDER BY vehicle.plates";

		this.rows = this.getQuery(SQLString);

		return this.rows;
	}

}
