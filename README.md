
TECNICAL ASSIGNMENT
----------------------------------------

On this little project, the technical assignment is succesfully solved. Everithing can be checked on functional implementation (Point 3).

CONTENT
-------------------------------------
- [1. DataBase](#1-Database)
- [2. Query](#2-Query)
- [3. Develpment](#3-Development)

------------------------------------

# 1. Database

I selected a MySQL database for this development. After following all the instructions of the test, the result entity-reltion diagram is showed on next picture:

![alt text](img/diagram.png "Diagram")

After creating the DB on MySQL, we can see on below picture, the final result:

![alt text](img/database.png "Database")

# 2. Query

The query used for get the information related on test instuctions is the following:

```
SELECT vehicle.plates, 
company.id_type, 
company.id_number, 
company.company_name, 
COUNT(vehicledriver.id_vehicle) 
FROM vehicle 
INNER JOIN company ON vehicle.id_company = company.id 
INNER JOIN vehicledriver ON vehicle.id = vehicledriver.id_vehicle 
INNER JOIN driver ON vehicledriver.id_driver = driver.id 
GROUP BY vehicledriver.id_vehicle 
HAVING COUNT(vehicledriver.id_vehicle) > 1 
ORDER BY vehicle.plates
```

Executing the query, we can get the info en the picture below:

![alt text](img/query.png "Query")

# 3. Development

To develope this app, I use the technologies below:

- Angular 10 as Front-End FrameWork
- Spring Boot 2 as Back-End FrameWork
- Pure Java code to connect to DB
- MySQL db to save all the data

Tools Needed:

- Node JS Installed
- Angular CLI
- Java RE 8
- Java JDK 8
- Spring Boot 2.3
- Any MySQL provider, I used XAMPP.

In the image below, you can see the arquitecture used:

![Arquitecture](img/arquitecture.png "Arquitecture")

Instructions to run:

1. Download both src folders (Back and Front).
2. Start MySQL Database.
3. Load .sql file (path:technical-test-backend\src\main\resources\static)
4. Compile and Execute as a Sprig Boot project all the back-end code. I used eclipse for doing this.
5. Compile and Execute all the front-end. I used PS of Windows.
6. Make sure three connections keep runing at same ports:
    - MySQL : 3306
    - Spring Boot Rest : 8080
    - Angular : 4200 (Not necessary for working)

7. Go to Chrome and go to "localhost:4200/". There you should see the UI correctly.

On the link below, you can see the app deployed on Gitlab pages, with basic funcionality of the FRONT-END

On https://jntorresn.gitlab.io/datatools/



